# Design and Development of F-RTO algorithm in ns-3

Spurious retransmission timeouts result in unnecessary retransmission of the last window of
data and decrease the TCP performance. In this project, I will try to implement F-RTO
detection algorithms for detecting spurious TCP retransmission timeouts